﻿using System;

namespace Sort
{
    class Program
    {
        static void Main(string[] args)
        {
            int[] data =
            {
                10, 4, 2, 14, 67, 2, 11 //, 33, 1, 15, 304, 764, 780, 929, 946, 448, 395, 609, 513, 635, 944, 348, 749,
                //397, 283, 311, 692, 2, 704, 715, 841, 923, 258, 265, 2, 714, 376, 193, 240, 441, 657, 585, 197, 563,
                //303, 963, 199, 114, 478, 848, 761, 678, 863, 255, 401, 471, 656, 966, 547, 596, 331, 198, 634, 188, 154,
                //166, 618, 552, 2, 721, 196, 205, 992, 240, 61, 645, 444, 521, 551, 873, 481, 365, 647, 52, 352, 66, 117,
                //867, 95, 256, 530, 977, 70, 94, 740, 56, 882, 701, 879, 46, 543, 787
            };

            data = QuickSort(data, 0, data.Length - 1);

            foreach (var item in data)
            {
                Console.WriteLine(item);
            }
        }

        static int[] QuickSort(int[] array, int startIndex, int endIndex)
        {
            int i = startIndex; //границы тех чисел что слева
            var j = endIndex ; //границы тех чисел что справа
            var pivot = array[(startIndex+endIndex) / 2];
            while (i <= j)
            {
                while (array[i] < pivot) 
                    i++;
                while (array[j] > pivot) 
                    j--;

                if (i > j) 
                    continue;
                (array[i], array[j]) = (array[j], array[i]);
                i++;
                j--;
            }

            if (startIndex < j)
                QuickSort(array, startIndex, j);
            if (endIndex > i)
                QuickSort(array, i, endIndex);

            return array;
        }
    }
}